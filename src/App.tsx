import * as React from 'react';
import { Provider } from 'react-redux';
import Footer from './containers/FooterContainer';
import Header from './containers/HeaderContainer';
import GitIssuesContainer from './pages/GitIssuesContainer';
import store from './store';

import './App.scss';

class App extends React.Component {
  public render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Header />
          <GitIssuesContainer />
          <Footer />
        </div>
      </Provider>
    );
  }
}

export default App;
