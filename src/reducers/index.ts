
import { combineReducers } from 'redux';
import issues from './issues';
import project from './project';

const store = combineReducers({
    currentProject: project,
    gitIssues: issues,
});

export default store;
