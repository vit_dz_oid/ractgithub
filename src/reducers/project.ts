import { FULFILLED } from 'redux-promise-middleware';
import fetchProject from '../actions/fetchProject';
import { Project } from '../serializers/Project';

interface IGitProject {
    project?: Project
}

export  const defaultState: IGitProject = {
    project: new Project()
};

const gitIssues = (state:IGitProject = defaultState, action: {payload: any, type: string}) => {
    if (`${fetchProject}_${FULFILLED}` === action.type) {
        return Object.assign({}, state, { project : action.payload.data });
    }
    return state;
};

export default gitIssues;
