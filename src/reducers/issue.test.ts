import { Issue } from '../serializers/Issue';
import issues, { defaultState } from './issues';

describe('Given the issues reducer', () => {
  describe('and the reducer is called', () => {
    const testAction = {
      payload: { data: [] as Issue[] },
      type: '',
    };

    let currentIssusesState = defaultState;
    let newState: any;

    beforeEach(() => {
      newState = issues(currentIssusesState, testAction);
    });

    describe('and the FETCH_ISSUES_FULFILLED action is dispatched', () => {
      beforeAll(() => {
        currentIssusesState = defaultState;
        const issuesInstance = (new Array(5)).fill(new Issue());

        testAction.type = 'FETCH_ISSUES_FULFILLED';
        testAction.payload = { data: issuesInstance };        
      });

      it('should return a new state with fetched issues only', () => {        
        expect(newState).toEqual({ list: testAction.payload.data, filter: 'open' });
      });
    });
  });
});