import { Project } from '../serializers/Project';
import project, { defaultState } from './project';

describe('Given the project reducer', () => {
  describe('and the reducer is called', () => {
    const testAction = {
      payload: { data: {} },
      type: '',
    };

    let currentProjectState = defaultState;
    let newState: any;

    beforeEach(() => {
      newState = project(currentProjectState, testAction);
    });

    describe('and the FETCH_PROJECT_FULFILLED action is dispatched', () => {
      beforeAll(() => {
        currentProjectState = defaultState;

        const projectInstance = new Project();
        projectInstance.fullName = 'test';

        testAction.type = 'FETCH_PROJECT_FULFILLED';
        testAction.payload = { data: projectInstance };
      });

      it('should return a new state with fetched project only', () => {
        expect(newState).toEqual({ project: testAction.payload.data });
      });
    });
  });
});