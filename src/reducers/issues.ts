import { FULFILLED } from 'redux-promise-middleware';
import fetchIssues from '../actions/fetchIssues';
import { Issue } from '../serializers/Issue';

interface IGitIssues {
    filter: string;
    list: Issue[];
}

export const defaultState: IGitIssues = {
    filter: 'open',
    list: [],
};

const gitIssues = (state:IGitIssues = defaultState, action: {payload: any, type: string}) => {
    if (`${fetchIssues}_${FULFILLED}` === action.type) {
        return Object.assign({}, state, { list : action.payload.data });
    }
    return state;
};

export default gitIssues;
