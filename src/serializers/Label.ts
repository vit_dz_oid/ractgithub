import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('Label')
export class Label {
    @JsonProperty('name')
    public name: string = '';

    @JsonProperty('color')
    public color: string = '';
}