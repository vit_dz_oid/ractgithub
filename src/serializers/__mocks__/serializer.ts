const deserializeArray = (data: any, ...some: any[]) => data;
const deserializeObject = (data: any, ...some: any[]) => data;

export {
  deserializeArray,
  deserializeObject
};
