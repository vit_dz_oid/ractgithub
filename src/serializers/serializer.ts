import { JsonConvert } from "json2typescript";

const deserializeObject = (obj: object, constructor: { new (): any; }) => {
    const jsonConvert: JsonConvert = new JsonConvert();
    return jsonConvert.deserializeObject(obj, constructor);
}

const deserializeArray = (arr: object[], constructor: { new (): any; }) => arr.map((item) => deserializeObject(item, constructor))

export { deserializeObject, deserializeArray };
