import { JsonObject, JsonProperty } from 'json2typescript';
import { User } from './User';

@JsonObject('Project')
export class Project {
    @JsonProperty('owner')
    public owner: User = new User();
    
    @JsonProperty('full_name')
    public fullName: string = '';

    @JsonProperty('subscribers_count')
    public subscribersCount: number = 0;

    @JsonProperty('watchers')
    public watchers: number = 0;

    @JsonProperty('forks')
    public forks: number = 0;

    @JsonProperty('open_issues')
    public openIssues: number = 0;
}
