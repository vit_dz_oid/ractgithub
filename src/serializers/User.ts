import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject('User')
export class User {
    @JsonProperty('login')
    public login: string = '';

    @JsonProperty('url')
    public url: string = '';
}