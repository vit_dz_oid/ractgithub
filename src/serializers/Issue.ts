import { JsonObject, JsonProperty } from "json2typescript";
import { Label } from './Label';
import { User } from './User';

@JsonObject('Issue')
export class Issue {
    @JsonProperty('url')
    public url: string = '';

    @JsonProperty('title')
    public title: string = '';

    @JsonProperty('labels')
    public labels: Label[] = [];

    @JsonProperty('number')
    public number: number = 0;

    @JsonProperty('closed_at')
    public closedAt?: any = undefined;

    @JsonProperty('created_at')
    public createdAt?: any = undefined;

    @JsonProperty('comments')
    public comments: number = 0;

    @JsonProperty('user')
    public user: User = new User();

    @JsonProperty('repository_url')
    public repositoryUrl: string = '';
}