import * as React from 'react';
import Issue from '../components/issue/Issue';
import IssueControls from '../components/issues-controls/IssuesControls';
import Label from '../components/label/Label';

import './style.scss';

export interface IProps {
    openedIssues?: number;
    closedIssues?: number;
    issues?: any[];
}

const GitIssues: React.StatelessComponent<IProps> = ({ openedIssues = 0, closedIssues = 0, issues = [] }) => {
    return (<div className="git-issues">
        <div className="container">
            <IssueControls />
        </div>
        <div className="container issues-container">
            <div className="issues-container-header">
                <Label text={`${openedIssues} Open`} icon="warning" />
                <Label text={`${closedIssues} Closed`} icon="check" />
            </div>
            <div className="issues-list row">
                {issues.map((issue, i) => (<Issue key={i} issue={issue} className="issue-item" />))}
            </div>
        </div>
    </div>);
}

export default GitIssues;
