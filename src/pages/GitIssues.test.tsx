import * as React from 'react';
import * as renderer from 'react-test-renderer';
import GitIssues, { IProps } from './GitIssues';

it('renders correctly GitIssues', () => {
  const props = {} as IProps;

  const tree = renderer
    .create(<GitIssues { ...props } />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});