import { connect } from 'react-redux';
import {
    compose,
    lifecycle,
    setDisplayName,
} from 'recompose';
import { bindActionCreators } from 'redux';
import fetchIssues from '../actions/fetchIssues';
import getIssues from '../selectors/getIssues';
import getProject from '../selectors/getProject';
import GitIssues, { IProps } from './GitIssues';

interface IDispatchers {
    dispatchFetchIssues: () => void,
}

const enhance = compose(
    setDisplayName('GitIssuesContainer'),
    connect(state => ({
        issues: getIssues(state),
        openedIssues: getProject(state).openIssues
    } as IProps), dispatch => bindActionCreators(
        {
            dispatchFetchIssues: fetchIssues,
        },
        dispatch,
    )),
    lifecycle({
        componentDidMount() {
            const { dispatchFetchIssues } = this.props as IDispatchers & IProps ;

            dispatchFetchIssues();
        }
    })
);

export default enhance(GitIssues);
