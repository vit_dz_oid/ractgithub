import { createAction } from 'redux-actions';
import { getProject } from '../api';
import { Project } from '../serializers/Project';
import { deserializeObject } from '../serializers/serializer';

const payloadCreator = () => getProject().then((resp) => ({ ...resp, data: deserializeObject(resp.data, Project) }));;

const fetchProjectAsync = createAction('FETCH_PROJECT', payloadCreator);

const fetchProject = () => (dispatch: any) => dispatch(fetchProjectAsync());

fetchProject.toString = fetchProjectAsync.toString;

export default fetchProject;