import { createAction } from 'redux-actions';
import { getIssues } from '../api';
import { Issue } from '../serializers/Issue';
import { deserializeArray } from '../serializers/serializer';

// TODO: move to API directory + enable availability headers attaching
const payloadCreator = () => getIssues().then((resp) => ({ ...resp, data: deserializeArray(resp.data, Issue) }));

const fetchIssuessAsync = createAction('FETCH_ISSUES', payloadCreator);

const fetchIssues = () => (dispatch: any) => dispatch(fetchIssuessAsync());

fetchIssues.toString = fetchIssuessAsync.toString;

export default fetchIssues;