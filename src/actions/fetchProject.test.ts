import fetchProject from './fetchProject';

jest.mock('../serializers/serializer');
jest.mock('../api');

describe('Given the fetchProject action', () => {
  describe('when the action is called', () => {
    let thunk: (dispatch: any) => any;

    beforeEach(() => {
      thunk = fetchProject();
    });

    it('should return a thunk function', () => {
      expect(thunk).toBeInstanceOf(Function);
    });

    describe('and the thunk is called', () => {
      let dispatchMock: () => void;
      let action: { type: string, payload: any};

      beforeEach(() => {
        dispatchMock = jest.fn(_ => _);

        action = thunk(dispatchMock);
      });

      it('should dispatch the FETCH_PROJECT async action', () => {
        expect(dispatchMock).toHaveBeenCalledTimes(1);
        expect(action).toEqual({
          payload: Promise.resolve(),
          type: 'FETCH_PROJECT',
        });
      });
    });
  });
});