import * as React from 'react';
import './style.scss';

interface IProps {
  children?: any;
  className?: string;
}

const ButtonsRow: React.StatelessComponent<IProps> = ({ children = null, className = "" }) => {
  return (
    <div className={`buttons-row ${className}`}>
      {children}
    </div>
  )
}

export default ButtonsRow;
