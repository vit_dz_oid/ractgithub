import * as React from 'react';
import * as renderer from 'react-test-renderer';
import ButtonsRow from './ButtonsRow';

it('renders correctly ButtonsRow', () => {
  const tree = renderer
    .create(<ButtonsRow />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
