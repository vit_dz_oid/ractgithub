import * as React from 'react';
import * as renderer from 'react-test-renderer';
import Issue from './Issue';

it('renders correctly Issue', () => {
  const tree = renderer
    .create(<Issue />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});