import * as moment from 'moment';
import * as React from 'react';
import { Issue as IssueModel } from '../../serializers/Issue';
import Icon from '../icons/Icon';
import IfExists from '../if-exists/IfExists';
import './style.scss';

interface IProps {
    issue?: IssueModel,
    className?: string;
}

const Issue: React.StatelessComponent<IProps> = ({ issue = new IssueModel(), className }) => {
    return (
        <div className={`issue row ${className}`}>
            <div className="icon-container col-1 col-2-sm">
                <Icon type="warning"/>
            </div>
            <div className="issue-description col-8 col-10-sm">
                <span className="issue-title">
                    <a href={issue.url}>{issue.title}</a>
                    {issue.labels.map((label: any, i: number) =>
                        (<span key={i} className="label" style={{ backgroundColor: `#${label.color}` }}>{label.name}</span>))
                    }
                </span>
                <br />
                <span className="issue-info">
                    #{issue.number} opened {moment(issue.createdAt).fromNow()} by <a href={issue.user.url}>{issue.user.login}</a>
                </span>
            </div>
            <div className="issue-discussing col-4 col-12-sm right">
                <IfExists statment={!!issue.comments}>
                    <a href="#comment">
                        <Icon type="comment" />
                        <span>{issue.comments}</span>
                    </a>
                </IfExists>
            </div>
        </div>
    );
}

export default Issue;