import * as React from 'react';
import * as renderer from 'react-test-renderer';
import IssuesControls, { IProps } from './IssuesControls';

it('renders correctly IssuesControls', () => {
  const props = {} as IProps;

  const tree = renderer
    .create(<IssuesControls { ...props } />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});