import * as React from 'react';
import ButtonsRow from '../buttons-row/ButtonsRow';
import Card from '../card/Card';
import './styles.scss';

export interface IProps {
  searchText?: string
}

const IssueControls: React.StatelessComponent<IProps> = ({ searchText = 'is:issue is:open' }) => {
  return (
    <div className="issues-controls">
      <div className="input-wrapper">
        <details>
          <summary className="filters">Filters</summary>
          <div className="details-content">
            <Card>
              <ul>
                <li>
                  <a href="#open">Open issuees anf pull requsts</a>
                </li>
                <li>
                  <a href="#my">Your issues</a>
                </li>
                <li>
                  <a href="#pr">Your pull request</a>
                </li>
              </ul>
            </Card>
          </div>
        </details>
        <input className="search-input" type="text" value={searchText} />
      </div>
      <div className="central">
        <ButtonsRow>
          <a className="navaigation-item" href='#'>Labels</a>
          <a className="navaigation-item" href='#'>Milestones</a>
        </ButtonsRow>
      </div>
      <a href="#" className="new-issue">New Issue</a>
    </div>
  )
}

export default IssueControls
