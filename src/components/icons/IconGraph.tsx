import * as React from 'react'
import { ICommonIconProps } from './icon-props.interface';

const IconGraph: React.StatelessComponent<ICommonIconProps> = ({ height = 16, width = 16, fill = '' }) => {
    return (
        <svg viewBox="0 0 16 16" version="1.1" height={height} width={width} fill={fill}>
            <path fillRule="evenodd" d="M16 14v1H0V0h1v14h15zM5 13H3V8h2v5zm4 0H7V3h2v10zm4 0h-2V6h2v7z" />
        </svg>
    )
}

export default IconGraph;