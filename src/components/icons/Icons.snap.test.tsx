import * as React from 'react';
import * as renderer from 'react-test-renderer';
import IconArrows from './IconArrows';
import IconBook from './IconBook';
import IconBookOpened from './IconBookOpened';
import IconCheck from './IconCheck';
import IconComment from './IconComment';
import IconEye from './IconEye';
import IconFork from './IconFork';
import IconGithub from './IconGithub';
import IconGraph from './IconGraph';
import IconMenu from './IconMenu';
import IconProject from './IconProject';
import IconPullRequest from './IconPullRequest';
import IconStar from './IconStar';
import IconWarning from './IconWarning';

it('renders correctly IconArrows', () => {
  const tree = renderer
    .create(<IconArrows />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly IconBook', () => {
  const tree = renderer
    .create(<IconBook />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly IconBookOpened', () => {
  const tree = renderer
    .create(<IconBookOpened />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly IconCheck', () => {
  const tree = renderer
    .create(<IconCheck />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly IconComment', () => {
  const tree = renderer
    .create(<IconComment />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly IconEye', () => {
  const tree = renderer
    .create(<IconEye />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly IconFork', () => {
  const tree = renderer
    .create(<IconFork />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly IconGithub', () => {
  const tree = renderer
    .create(<IconGithub />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly IconGraph', () => {
  const tree = renderer
    .create(<IconGraph />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly IconMenu', () => {
  const tree = renderer
    .create(<IconMenu />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly IconProject', () => {
  const tree = renderer
    .create(<IconProject />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly IconPullRequest', () => {
  const tree = renderer
    .create(<IconPullRequest />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly IconStar', () => {
  const tree = renderer
    .create(<IconStar />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly IconWarning', () => {
  const tree = renderer
    .create(<IconWarning />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
