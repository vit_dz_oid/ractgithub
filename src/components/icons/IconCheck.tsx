import * as React from 'react'
import { ICommonIconProps } from './icon-props.interface';

const IconCheck: React.StatelessComponent<ICommonIconProps> = ({ height = 16, width = 12, fill = '' }) => {
    return (
        <svg viewBox="0 0 12 16" version="1.1" height={height} width={width} fill={fill}>
            <path fillRule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5L12 5z" />
        </svg>
    )
}

export default IconCheck;