import * as React from 'react'
import { IIconProps } from './icon-props.interface';
import IconArrows from './IconArrows';
import IconBook from './IconBook';
import IconBookOpened from './IconBookOpened';
import IconCheck from './IconCheck';
import IconComment from './IconComment';
import IconEye from './IconEye';
import IconFork from './IconFork';
import IconGithub from './IconGithub';
import IconGraph from './IconGraph';
import IconMenu from './IconMenu';
import IconProject from './IconProject';
import IconPullRequest from './IconPullRequest';
import IconStar from './IconStar';
import IconWarning from './IconWarning';

export const icons = {
    arrows: IconArrows,
    book: IconBook,
    bookOpened: IconBookOpened,
    check: IconCheck,
    comment: IconComment,
    eye: IconEye,
    fork: IconFork,
    github: IconGithub,
    graph: IconGraph,
    menu: IconMenu,
    project: IconProject,
    pullRequest: IconPullRequest,
    star: IconStar,
    warning: IconWarning,
};

const Icon: React.StatelessComponent<IIconProps> = (props) => {
    const Element = icons[props.type] || null;
    const { fill } = props;

    return Element ? <Element {...props} fill={ fill ? fill : 'currentColor'} /> : null;
}

export default Icon;
