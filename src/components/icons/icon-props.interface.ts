export interface ICommonIconProps {
    height?: number;
    width?: number;
    fill?: string;
    hoverColor?: string;
};

export interface IIconProps extends ICommonIconProps {
    type: string;
}
