import * as React from 'react';
import * as renderer from 'react-test-renderer';
import IfExists, { IProps } from './IfExists';

it('renders correctly IfExists', () => {
  const props = {} as IProps;

  const tree = renderer
    .create(<IfExists { ...props } />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly with false statment IfExists', () => {
  const tree = renderer
    .create(<IfExists statment={false}>Test</IfExists>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly with true statment IfExists', () => {
  const tree = renderer
    .create(<IfExists statment={true}>Test</IfExists>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});