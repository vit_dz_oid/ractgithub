import * as React from 'react';

export interface IProps {
    children: any;
    statment: boolean;
}

const IfExists: React.StatelessComponent<IProps> = ({ children = null, statment = false}) => {
    return statment ? children : null;
}

export default IfExists;