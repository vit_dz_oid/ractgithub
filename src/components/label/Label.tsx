import * as React from 'react';
import Icon from '../icons/Icon';
import './style.scss';

export interface IProps {
    icon?: string;
    text?: string;
    iconPosition?: 'left' | 'right';
    className?: string;
} 

const  Label: React.StatelessComponent<IProps> = ({
  icon = null,
  text = null,
  iconPosition = 'left',
  className = ''
}) =>{
    return (
      <label className={`label ${className}`}>
        {icon ? <span style={{ float: iconPosition }}><Icon type={icon}>{icon}</Icon></span> : null}
        {text}
      </label>
    );
}

export default Label;