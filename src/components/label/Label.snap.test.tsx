import * as React from 'react';
import * as renderer from 'react-test-renderer';
import Label, { IProps } from './Label';

it('renders correctly Label', () => {
  const props = {} as IProps;

  const tree = renderer
    .create(<Label { ...props } />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});