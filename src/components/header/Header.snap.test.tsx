import * as React from 'react';
import * as renderer from 'react-test-renderer';
import Header, { IProps } from './Header';

it('renders correctly with props Header', () => {
  const props: IProps = {
    author: '',
    forks: 0,
    project: '',
    stars: 0,
    tabs: [],
    watches: 0,
};

  const tree = renderer
    .create(<Header { ...props }/>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly without props Header', () => {
  const props: IProps = {} as IProps;

  const tree = renderer
    .create(<Header { ...props }/>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});