import * as React from 'react';
import Icon from '../icons/Icon';
import IconEye from '../icons/IconEye';
import IconFork from '../icons/IconFork';
import IconStar from '../icons/IconStar';
import NavigationTabs from '../navaigation-tabs/NavigationTabs';

import './styles.scss';

export interface IProps {
    author: string;
    forks: number;
    stars: number;
    watches: number;
    project: string;
    tabs: any[];
}

const Header: React.StatelessComponent<IProps> = ({
    author = 'facebook',
    forks = 0,
    stars = 0,
    watches = 0,
    project = 'react',
    tabs = []
}) => {
    return (<div className="header">
        <div className="desktop">
            <div className="container tablet-container">
                <div className="project-info">
                    <div className="project-description">
                        <Icon type="book" />
                        <span><a className="link-author" href="#">{author}</a>/<a className="link-project" href="#">{project}</a></span>
                    </div>
                    <div className="statistics">
                        <div className="actions">
                            <button className="action-button">
                                <IconEye />
                                <span>Watch</span>
                            </button>
                            <label>{watches}</label>
                        </div>
                        <div className="actions">
                            <button className="action-button">
                                <IconStar />
                                <span>Star</span>
                            </button>
                            <label>{stars}</label>
                        </div>
                        <div className="actions">
                            <button className="action-button">
                                <IconFork />
                                <span>Fork</span>
                            </button>
                            <label>{forks}</label>
                        </div>
                    </div>
                </div>
                <NavigationTabs tabs={tabs} />
            </div>
        </div>
        <div className="mobile">
            <div className="mobile-menu">
                <div className="project-description-mobile">
                    <a className="link-author" href="#">{author}</a>/<a className="link-project" href="#">{project}</a>
                </div>
                <details>
                    <summary className="top-menu-row">
                        <Icon type="menu" height={20} width={18} />
                    </summary>
                    <div className="expanded-menu-content">
                        <NavigationTabs tabs={tabs}/>
                    </div>
                </details>
            </div>
        </div>
    </div >);
}

export default Header;
