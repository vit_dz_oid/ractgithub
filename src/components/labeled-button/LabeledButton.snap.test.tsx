import * as React from 'react';
import * as renderer from 'react-test-renderer';
import LabeledButton, { IProps } from './LabeledButton';

it('renders correctly LabeledButton', () => {
  const props = {} as IProps;

  const tree = renderer
    .create(<LabeledButton { ...props } />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
