import * as React from 'react';

import './styles.scss';

export interface IProps {
    buttonPart: any;
    labelPart: any;
}

const LabeledButton: React.StatelessComponent<IProps> = ({ buttonPart = null, labelPart = null }) => {
    return (<div className="labeled-button">
        <div className="button-part">{buttonPart}</div>
        <div className="label-part">{labelPart}</div>
    </div>);
}

export default LabeledButton;
