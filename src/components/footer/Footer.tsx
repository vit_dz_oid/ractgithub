import * as React from 'react';
import Icon from '../icons/Icon';

import './styles.scss';

interface ILink {
    link: string;
    name: string;
};

interface IProps {
    links: ILink[]
}

const Footer: React.StatelessComponent<IProps> = ({ links = [] }) => {
    const [links1, links2] = links.reduce((prev: ILink[][], current: ILink, index: number) => {
        prev[links.length / 2 < (index + 1) ? 0 : 1].push(current);
        return prev;
    }, [[], []]);
    
    return (
        <div className="footer">
            <div className="container footer-container">
                <div className="fototer-content">
                    <ul className="links-left">
                        <li >© 2018 <span>GitHub</span>, Inc.</li>
                        {links1.map((el: ILink, i: number) => (<li key={i}><a href={el.link}>{el.name}</a></li>))}
                    </ul>
                    <a className="github-link" title="GitHub" href="https://github.com">
                        <Icon type="github" width={24} height={24} />
                    </a>
                    <ul className="links-right">
                        {links2.map((el: ILink, i: number) => (<li key={i}><a href={el.link}>{el.name}</a></li>))}
                    </ul>
                </div>
            </div>
        </div >
    );
}

export default Footer;
