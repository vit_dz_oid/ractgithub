import * as React from 'react';
import * as renderer from 'react-test-renderer';
import { FOOTER_LINKS } from '../../config/footer-links';
import Footer from './Footer';

it('renders correctly without links Footer', () => {
  const tree = renderer
    .create(<Footer links={[]} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly with links Footer', () => {
  const tree = renderer
    .create(<Footer links={FOOTER_LINKS} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});