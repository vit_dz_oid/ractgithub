import * as React from 'react';
import * as renderer from 'react-test-renderer';
import NavigationTabs, { IProps } from './NavigationTabs';

it('renders correctly NavigationTabs', () => {
  const props = {} as IProps;

  const tree = renderer
    .create(<NavigationTabs { ...props } />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});