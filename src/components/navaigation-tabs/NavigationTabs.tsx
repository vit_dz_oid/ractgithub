import * as React from 'react';
import Icon from '../icons/Icon';

import './styles.scss';

export interface IProps {
    active?: string;
    tabs?: ITab[];
}
interface ITab {
    additional: string;
    link: string;
    name: string;
    icon: string;
};

const NavigationTabs: React.StatelessComponent<IProps> = ({
    tabs = [],
    active = 'Issues'
}) => {
    return (<nav className='navigation-tabs'>
        {tabs.map(({ additional, link, name, icon }, i: number) =>
            (
                <a key={i} className={`navigation-tab-link ${active === name ? 'active' : ''}`} href={link}>
                    <div className="icon-wrapper">
                        <Icon type={icon} />
                    </div>
                    {name}
                    {additional ? <label className="count">{additional}</label> : null}
                </a>
            )
        )}
    </nav>);
}

export default NavigationTabs;
