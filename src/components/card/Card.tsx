import * as React from 'react';
import './Card.scss';

interface IProps {
  children?: any
}

const Card: React.StatelessComponent<IProps> = ({ children = null }) => {
  return (
    <div className="card">
      {children}
    </div>
  )
}

export default Card
