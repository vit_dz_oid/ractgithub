const getIssues = () => Promise.resolve({ data: [] })
const getProject = () => Promise.resolve({ data: {} });

 export {
   getIssues,
   getProject
 };
