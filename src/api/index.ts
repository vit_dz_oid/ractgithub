import axios, { AxiosRequestConfig } from "axios";

const API_URL = 'https://api.github.com/repos/facebook/react';
const requestInstance = axios.create({
    baseURL: API_URL
});

const getIssues = (): Promise<any> => {
    return request({
        params: {
            status: 'open'
        },
        url: '/issues',
    });
}

const getProject = (): Promise<any> => {
    return request({
        url: '',
    });
}

async function request(config: AxiosRequestConfig) {
    try {
        return requestInstance.request(config);
    }
    catch (e) {
        return Promise.reject(console.warn('Smth happens with the request! Err: ', e));
    }
}

export {
    getIssues,
    getProject,
}
