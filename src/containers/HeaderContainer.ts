import { connect } from 'react-redux';
import {
    compose,
    lifecycle,
    setDisplayName,
} from 'recompose';
import { bindActionCreators } from 'redux';
import fetchProject from '../actions/fetchProject';
import Header, { IProps } from '../components/header/Header';
import { NAV_TABS } from '../config/navigation-tabs';
import { IObject } from '../interfaces/IObject';
import getProject from '../selectors/getProject';
import { Project } from '../serializers/Project';

interface IDispatchers {
    dispatchGetProject: () => void,
}

const enhance = compose(
    setDisplayName('HeaderComponent'),
    connect(
        (state: IObject): IProps => {
            const { forks, subscribersCount: watches, watchers: stars, fullName = '', openIssues } = getProject(state) as Project;
            const [author, project] = fullName.split('/');

            return {
                author,
                forks,
                project,
                stars,
                tabs: NAV_TABS.map(item => {
                    if (item.name === 'Issues') {
                        return { ...item, additional: openIssues }
                    }

                    return item;
                }),
                watches,
            }
        },
        dispatcher => bindActionCreators({
            dispatchGetProject: fetchProject
        }, dispatcher)
    ),
    lifecycle({
        componentDidMount() {
            const { dispatchGetProject } = this.props as IDispatchers;

            dispatchGetProject();
        }
    })
);


export default enhance(Header);
