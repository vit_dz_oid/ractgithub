import {
  compose,
  defaultProps,
  setDisplayName,
} from 'recompose';
import Footer from '../components/footer/Footer';
import { FOOTER_LINKS } from '../config/footer-links';

const enhance = compose(
  setDisplayName('FooterComponent'),
  defaultProps({
    links: FOOTER_LINKS
  })
);

export default enhance(Footer);
