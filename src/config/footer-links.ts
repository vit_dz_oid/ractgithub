export const FOOTER_LINKS = [
  {
    link: 'https://github.com/site/terms',
    name: 'Terms',
  },
  {
    link: 'https://github.com/site/privacy',
    name: 'Privacy',
  },
  {
    link: 'https://help.github.com/articles/github-security/',
    name: 'Security',
  },
  {
    link: 'https://status.github.com/',
    name: 'Status',
  },
  {
    link: 'https://help.github.com',
    name: 'Help',
  },
  {
    link: 'https://github.com/site/terms',
    name: 'Contact GitHub',
  },
  {
    link: 'https://github.com/site/privacy',
    name: 'Pricing',
  },
  {
    link: 'https://help.github.com/articles/github-security/',
    name: 'API',
  },
  {
    link: 'https://status.github.com/',
    name: 'Training',
  },
  {
    link: 'https://blog.github.com',
    name: 'Blog',
  },
  {
    link: 'https://github.com/about',
    name: 'About',
  }
];
