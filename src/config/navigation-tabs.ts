export const NAV_TABS = [
    {
        icon: 'arrows',
        link: '#code',
        name: 'Code',
    },
    {
        icon: 'warning',
        link: '#issues',
        name: 'Issues',
    },
    {
        icon: 'pullRequest',
        link: '#pull_requests',
        name: 'Pull requests',
    },
    {
        icon: 'project',
        link: '#projects',
        name: 'Projects',
    },
    {
        icon: 'bookOpened',
        link: '#wiki',
        name: 'Wiki',
    },
    {
        icon: 'graph',
        link: '#insights',
        name: 'Insights',
    },
]; 