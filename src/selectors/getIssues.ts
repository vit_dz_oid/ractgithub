import { createSelector } from 'reselect';
import { IObject } from 'src/interfaces/IObject';
import { Issue } from 'src/serializers/Issue';

export const getIssues = (state: IObject) => state.gitIssues.list;

const issues = createSelector(
    getIssues,
    (list: any[]): Issue[] => list
);

export default issues;
