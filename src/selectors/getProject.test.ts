import { Project } from '../serializers/Project';
import getProject from './getProject';

describe('Given the getProject selector', () => {
  const project  = new Project();

  let store = {
    currentProject: {
      project
    }
  };

  describe('when the selector is called', () => {
    let result: Project;

    beforeEach(() => {
      result = getProject(store);
    });

    describe('store with default project', () => {
      it('should return project with default fullName', () => {
        expect(result.fullName).toBe('')
      });
    });

    describe('project in the store', () => {
      beforeAll(() => {
        const projectInstance = new Project();
        projectInstance.fullName = 'test';

        store = { currentProject: { project: projectInstance }};
      })

      it('should return project with specified fullName', () => {
        expect(result.fullName).toBe('test')
      });
    });
  });
});
