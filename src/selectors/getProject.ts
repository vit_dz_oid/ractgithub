import { createSelector } from 'reselect';
import { IObject } from 'src/interfaces/IObject';
import { Project } from 'src/serializers/Project';

const getProjectFromStore = (state: IObject) => state.currentProject;

const project = createSelector(
    getProjectFromStore,
    (proj: IObject): Project => proj.project
);

export default project;
