import { Issue } from '../serializers/Issue';
import getIssues from './getIssues';

describe('Given the getIssues selector', () => {
  const list: Issue[] = [];

  let store = {
    gitIssues: {
      list
    }
  };

  describe('when the selector is called', () => {
    let result: Issue[];

    beforeEach(() => {
      result = getIssues(store);
    });

    describe('store with default issues', () => {
      it('should return empty issues array', () => {
        expect(result).toBeInstanceOf(Array);
      });
    });

    describe('issues in the store', () => {
      beforeAll(() => {
        const issuesInstance = (new Array(5)).fill(new Issue());

        store = {
          gitIssues: {
            list: issuesInstance
          }
        };
      })

      it('should return project with 5 issues', () => {
        expect(result.length).toBe(5);
      });
    });
  });
});
